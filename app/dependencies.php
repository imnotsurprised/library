<?php


$container = $app->getContainer();

// Twig DIC
$container['view'] = function ($container) {
    $settings = $container['settings'];
    
    $view = new Slim\Views\Twig(
        $settings['view']['template_path'],
        $settings['view']['twig']
    );
    
    // Add extensions
    $view->addExtension(new Slim\Views\TwigExtension(
        $container->get('router'), 
        $container->get('request')->getUri()
    ));
    
    $view->addExtension(new Twig_Extension_Debug());
    
    return $view;
};

//PDO DIC
$container['db'] = function ($container) {
    $db = $container['settings']['db'];
    
    $pdo = new PDO(
        "mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
        $db['user'], $db['pass']
    );
    
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    
    return $pdo;
};

//PDO DIC
$container['errorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
        return $container['response']->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->write('Library tools error. Please contact from administrator');
    };
};

//Home Controller DIC
$container['HomeController'] = function($container) {
    $view = $container->get('view');
    $db = $container->get('db');
    return new \app\controllers\HomeController($view, $db);
};

//Ajax Controller DIC
$container['AjaxController'] = function($container) {
    $view = $container->get('view');
    $db = $container->get('db');
    return new \app\controllers\AjaxController($view, $db);
};