<?php

return [
    'settings' => [
        'displayErrorDetails' => false,
        
        // View settings
        'view' => [
            'template_path' => __DIR__ . '/views',
            'twig' => [
                'cache' => false
            ]
        ],
        
        // PDO settings
        'db' => [
            'host' => 'localhost',
            'user' => '',
            'pass' => '',
            'dbname' => 'library'
        ]
    ]
];