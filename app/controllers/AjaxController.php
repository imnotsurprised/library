<?php

namespace app\controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Class process ajax requests
 */

final class AjaxController extends BaseController
{
    /**
     * get available min and max year filter range
     * @param object $request includes request properties
     * @param object $response includes response properties
     * @return json $response If success return min and max, else error message.
     */
    public function getFilterRange(Request $request, Response $response)
    {
        $range = $this->bookObj->getFilterRange();
        
        if (empty($range))
        {
            return $response->withStatus(400)->withJSON(
                ['message' => 'Cant get correct range']
            );
        }
        
        return $response->withStatus(200)->withJSON(
            [
                'min' => date('Y', strtotime($range['min'])),
                'max' => date('Y', strtotime($range['max']))
            ]
        );
    }
    
    /**
     * filter author books data from client params
     * @param object $request includes request properties
     * @param object $response includes response properties
     * @return json $response If success return array from basic author books data, else error message.
     */
    public function filter(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        
        $booksList = $this->bookObj->filterBooksInfo($data);

        if (empty($booksList))
        {
            return $response->withStatus(400)->withJSON(
                ['message' => 'Books for current filter not found']
            );
        }
        
        return $response->withStatus(200)->withJSON($booksList);
    }
    
    /**
     * get all author books by client filter params
     * @param object $request includes request properties
     * @param object $response includes response properties
     * @return json $response If success return array from author books data, else error message.
     */
    public function viewFilteredBooks(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        
        $booksList = $this->bookObj->getFilteredBooksByAuthor($data);

        if (empty($booksList))
        {
            return $response->withStatus(400)->withJSON(
                ['message' => 'Books not found']
            );
        }
        
        return $response->withStatus(200)->withJSON($booksList);
    }
}