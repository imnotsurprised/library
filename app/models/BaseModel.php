<?php

namespace app\models;

/**
 * Base class. Configure properties for child models 
 */
abstract class BaseModel
{
    /**
     * PDO object from dic
     */
    protected $dbh;
    
    public function __construct($db) 
    {
        $this->dbh = $db;
    }
}