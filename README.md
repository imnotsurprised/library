Library tools. MVC construction.
The app allows you to get information about the authors and their books from filter.

Key directories

app: Application code
app/views: Twig template files
public: Webserver root
vendor: Composer dependencies

Key files

public/index.php: Entry point to application
app/settings.php: Configuration
app/dependencies.php: Services for Pimple
app/routes.php: All application routes are here
app/controllers: All controllers are here
app/models: All models are here

INSTALLATION

git clone https://imnotsurprised@bitbucket.org/imnotsurprised/library.git
php composer install

cd build
in file liquibase.properties set you mysql credentials
./db_create.sh

cd app
in file settings.php set you mysql credentials

php -S 127.0.0.1:8080 -t public/
open browser url: http://127.0.0.1:8080

If You Need Help

If you have problems using or install system, please write in email(simple.vladimir.shtefan@gmail.com), and I will try to help you.

License

The library tools is free software distributed under the terms of the MIT license.

Social Links

https://www.linkedin.com/in/vladimir-shtefan-36ba6b85/