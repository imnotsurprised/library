App.modules.Helper = {
    modalObj: {},
    
    ajax: function(type, url, data) {
        var _self = this;
        return $.ajax({
            url: url + '?xcache=' + new Date().getTime(),
            type: type,
            data: data || '',
            dataType: 'json',
            async: false,
            beforeSend: function() {
                _self.overlay('show');  
            }
        }).always(function() {
            _self.overlay('hide');
        });
    },
    
    overlay: function(action) {
        ('show' === action) ? $('.overlay').show() : $('.overlay').hide();
    },
    
    modal: function(settings) {
        $('#dialog-content').empty();
        
        this.modalObj = new jBox('Modal', {
            closeOnEsc: true,
            closeButton: 'title',
            closeOnClick: 'title',
            title: settings.title,
            width: settings.width || 'auto',
            maxWidth: settings.width || 'auto',
            height: settings.height || 'auto',
            maxHeight: settings.height || 'auto',
            zIndex: 100,
            animation: 'zoomIn',
            content: $('#dialog-content')
        });
    },
    
    modalAction: function(action) {
        ('open' === action) ? this.modalObj.open() : this.modalObj.close();
    },
    
    notice: function(text, color, position) {
        new jBox('Notice', {
            theme: 'NoticeFancy',
            content: text,
            color: color,
            attributes: position || {x: 'right', y: 'top'},
            animation: {
                open: 'tada',
                close: 'zoomIn'
            },
            autoClose: 4000,
            stack: false,
            volume: 80,
            audio: '/audio/bling1'
        });
    }
};