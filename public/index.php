<?php

require_once __DIR__ . '/../vendor/autoload.php';

// Instantiate the app
$settings = require_once __DIR__ . '/../app/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require_once __DIR__ . '/../app/dependencies.php';

// Register routes
require_once __DIR__ . '/../app/routes.php';

$app->run();