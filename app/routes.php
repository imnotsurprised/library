<?php

$app->get('/', 'HomeController:index');
$app->post('/ajax/books-filter', 'AjaxController:filter');
$app->post('/ajax/view-filtered-books', 'AjaxController:viewFilteredBooks');
$app->get('/ajax/available-range', 'AjaxController:getFilterRange');