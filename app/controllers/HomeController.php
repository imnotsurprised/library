<?php

namespace app\controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Class render main page
 */

final class HomeController extends BaseController
{
    /**
     * function render main page
     * @param object $request includes request properties
     * @param object $response includes response properties
     * @return object $response prepare object from the rendered template
     */
    public function index(Request $request, Response $response)
    {
        $booksList = $this->bookObj->getDefaultBooksList();
        $this->view->render($response, 'home.twig', ['books' => $booksList]);
        return $response;
    }
}