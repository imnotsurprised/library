<?php

namespace app\controllers;

use app\models\Book;

/**
 * Base class. Configure properties for child controllers
 */

abstract class BaseController
{
    /**
     * view object from dic
     */
    protected $view;
    
    /**
     * object for working with book model
     */
    protected $bookObj;

    public function __construct($view, $db)
    {
        $this->view = $view;
        $this->bookObj = new Book($db);
    }
}