var App = {
    filter: {},
    
    init: function() {   
        this.prepareYearRange();
        
        $('#filter-form').submit(function(e) {
            e.preventDefault();
            App.handleFilter();
        });
        
        $('.book-view-btn').click(function() {
            App.viewAuthorBooks(this);
        });
    },
    
    viewAuthorBooks: function(element) {
        if ($(element).attr('author-book-count') == 0) {
            App.modules.Helper.notice('Books for current author not found', 'blue');
            return;
        }

        var data = {};
        data.authorID = $(element).attr('author-id');
        data.filter = this.filter;

        var req = App.modules.Helper.ajax('POST', '/ajax/view-filtered-books', data);
        req.success(function(response) {
            App.modules.Helper.modal({title: 'View author books'});
            
            $('#dialog-content').load('templates/author-books.html', function() {
                $.each(response, function(key, book) {
                    $('#author-books-list').find('tbody').append(
                        $('<tr>')
                            .append($('<td>').append(key +1))
                            .append($('<td>').append(book.title))
                            .append($('<td>').append(book.published))
                            .append($('<td>').append(book.isbn))
                            .append($('<td>').append(book.pages))
                    );
                });
                
                App.modules.Helper.modalAction('open');
            });
        });
        req.error(function(xhr) {
            App.modules.Helper.notice(xhr.responseJSON.message, 'red');
        });
    },
    
    prepareYearRange: function() {
        var req = App.modules.Helper.ajax('GET', '/ajax/available-range');
        req.success(function(response) {
            $('#inputFrom, #inputTo').datepicker({
                format: "yyyy",
                autoclose: true,
                minViewMode: "years",
                defaultViewDate: 'year',
                startDate: response.min,
                endDate: response.max
            });
        });
        req.error(function(xhr) {
            App.modules.Helper.notice(xhr.responseJSON.message, 'red');
        });
    },
    
    handleFilter: function() {
        this.filter = {};
        
        var author = $.trim($('#inputAuthor').val());
        var from = $("#inputFrom").data('datepicker').getFormattedDate('yyyy');
        var to = $("#inputTo").data('datepicker').getFormattedDate('yyyy');
        
        if ( from === '' && to !== '' || from !== ''&& to === ''  || to < from) {
            App.modules.Helper.notice('Please specify correct range', 'red');
            return;
        }
        
        if (author !== '') {
            this.filter.author = author;
        }
        if (from !== '' && from !== '') {
            this.filter.from = from;
            this.filter.to = to;
        }
        
        if (!$.isEmptyObject(this.filter)) {
            var req = App.modules.Helper.ajax('POST', 'ajax/books-filter', this.filter);
            req.success(function(response) {
                $('#books-list').find('tbody').empty();
                
                $.each(response, function(key, book) {
                    var viewButton = $('<a class="book-view-btn" author-book-count="'+book.numberOfBooks+'" author-id="'+book.author_id+'"><span class="glyphicon glyphicon-book" data-toggle="tooltip" title="View Author Books"></span></a>');
                    $(viewButton).click(function() {
                        App.viewAuthorBooks(this);
                    });
                    
                    $('#books-list').find('tbody').append(
                        $('<tr>')
                            .append($('<td>').append(key +1))
                            .append($('<td>').append(book.author))
                            .append($('<td>').append(book.numberOfBooks))
                            .append($('<td>').append(viewButton))
                    );
                });
            });
            req.error(function(xhr) {
                App.modules.Helper.notice(xhr.responseJSON.message, 'red');
                setTimeout(function() {
                    $('#filter-form')[0].reset();
                    window.location.reload();
                }, 3000);
            });
        }
        else {
            App.modules.Helper.notice('Please specify filter params', 'red');
        } 
    }
};

App.modules = {};