<?php

namespace app\models;

/**
 * Class for working with book entity.
 */
class Book extends BaseModel
{
    /**
     * get available year range for filter
     * @return array min and max available value
     */
    public function getFilterRange()
    {
        $sql = 'SELECT
                    min(`published`) as min,
                    max(`published`) as max 
                FROM
                    book';
        
        $stmt = $this->dbh->prepare($sql);
        $stmt->execute();

        return $stmt->fetch();
    }
    
    /**
     * Get all author list books
     * @return array author_id, author, numberOfBooks values
     */
    public function getDefaultBooksList()
    {
        $sql = "SELECT 
                    A.author_id,
                    CONCAT(A.first_name, ' ', A.surname) as author,
                    COUNT(B.book_id) as numberOfBooks
                FROM 
                    author as A
                LEFT JOIN 
                    author_book AB ON (AB.author_id = A.author_id AND AB.is_available != 0)
                LEFT JOIN 
                    book B ON (B.book_id = AB.book_id)
                GROUP BY 
                    A.author_id";

        $stmt = $this->dbh->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
    
    /**
     * filter author books info by client
     * @param array $data filter data available key author, from, to
     * @return array filtered list data
     */
    public function filterBooksInfo($data)
    {
        $prepareData = array();
        
        $sql = "SELECT 
                    A.author_id,
                    CONCAT(A.first_name, ' ', A.surname) as author,
                    COUNT(B.book_id) as numberOfBooks
                FROM 
                    author as A
                LEFT JOIN 
                    author_book AB ON (AB.author_id = A.author_id AND AB.is_available != 0)
                LEFT JOIN
                    book B ON (B.book_id = AB.book_id)";

        if (isset($data['author']) || isset($data['from']))
        {
            $sql .= ' WHERE';
        }

        if (isset($data['author']))
        {
            $sql .= " CONCAT(A.first_name, ' ', A.surname) LIKE :author";

            $prepareData[':author'] = '%'.$data['author'].'%';
        }

        if (isset($data['author']) && isset($data['from']))
        {
            $sql .= ' AND';
        }

        if (isset($data['from']))
        {
            $lastMonthDay =  cal_days_in_month(CAL_GREGORIAN, 12, $data['to']);
            $start = "{$data['from']}-01-01 00:00:00";
            $end = "{$data['to']}-12-$lastMonthDay 23:59:59";

            $sql .= ' B.published BETWEEN :start AND :end';
            $prepareData[':start'] = $start;
            $prepareData[':end'] = $end;
        }

        $sql .= " GROUP BY A.author_id";

        $stmt = $this->dbh->prepare($sql);

        $stmt->execute($prepareData);

        return $stmt->fetchAll();
    }
    
    /**
     * get books for selected author by filter
     * @param array $data filter data available key authorID, from, to
     * @return array filtered list books
     */
    public function getFilteredBooksByAuthor($data)
    {
        $prepareData = array(
            'author_id' => $data['authorID']
        );
        
        $sql = 'SELECT
                    GROUP_CONCAT(B.title) as title,
                    B.published,
                    B.isbn,
                    GROUP_CONCAT(B.pages) as pages
                FROM
                    book as B,
                    author_book as AB
                WHERE
                    B.book_id = AB.book_id AND
                    AB.author_id = :author_id AND
                    AB.is_available != 0';
        
        if (isset($data['filter']['from']) && isset($data['filter']['to']))
        {
            $lastMonthDay =  cal_days_in_month(CAL_GREGORIAN, 12, $data['filter']['to']);
            $start = "{$data['filter']['from']}-01-01 00:00:00";
            $end = "{$data['filter']['to']}-12-$lastMonthDay 23:59:59";

            $sql .= ' AND B.published BETWEEN :start AND :end';
            $prepareData[':start'] = $start;
            $prepareData[':end'] = $end;
        }
        
        $sql .= ' GROUP BY B.isbn';
        
        $stmt = $this->dbh->prepare($sql);

        $stmt->execute($prepareData);

        return $stmt->fetchAll();
    }
}